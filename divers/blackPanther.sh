#!/bin/bash
options="$*"

rm -R /tmp/multisystem/multisystem-modinitrd 2>/dev/null
mkdir /tmp/multisystem/multisystem-modinitrd 2>/dev/null
#Décompresser
cd /tmp/multisystem/multisystem-modinitrd
gzip -dc "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/blackPanther/initrd.gz" | cpio -imvd --no-absolute-filenames

#read -s -n1 -p "Appuyez sur une touche pour continuer..."; echo

#Remplacer iso par vfat
sed -i "s@mount -n -t iso9660 -o ro \$realdev /live/media@mount -n -t vfat -o ro /dev/disk/by-uuid/$(cat /tmp/multisystem/multisystem-selection-uuid-usb) /live/media@g" /tmp/multisystem/multisystem-modinitrd/sbin/blackPanther-root

#modifier dossier du squashfs
sed -i "s@/live/media/loopbacks/@/live/media/blackPanther/@" /tmp/multisystem/multisystem-modinitrd/sbin/blackPanther-root

#sudo gedit /tmp/multisystem/multisystem-modinitrd/sbin/blackPanther-root
#read -s -n1 -p "Appuyez sur une touche pour continuer..."; echo

find . | sudo cpio -o -H newc | gzip -9 | tee "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/blackPanther/initrd.gz"
cd -
rm -R /tmp/multisystem/multisystem-modinitrd
exit 0
