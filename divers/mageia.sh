#!/bin/bash
options="$*"

#Décompresser
rm -R /tmp/multisystem/multisystem-modinitrd 2>/dev/null
mkdir /tmp/multisystem/multisystem-modinitrd 2>/dev/null
cd /tmp/multisystem/multisystem-modinitrd
xz --decompress --stdout "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/${options}/initrd.gz" | cpio -imvd --no-absolute-filenames

rm "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/${options}/initrd.gz"

#read -s -n1 -p "Appuyez sur une touche pour continuer..."; echo

#Mageia version liveCD
sed -i "s@mount -n -t iso9660 -o ro \$realdev /live/media@mount -n -t vfat -o ro /dev/disk/by-uuid/$(cat /tmp/multisystem/multisystem-selection-uuid-usb) /live/media@g" /tmp/multisystem/multisystem-modinitrd/sbin/mgalive-root

#modifier dossier du squashfs
sed -i "s@/live/media/loopbacks/@/live/media/${options}/@" /tmp/multisystem/multisystem-modinitrd/sbin/mgalive-root

#sudo gedit /tmp/multisystem/multisystem-modinitrd/sbin/mgalive-root
#read -s -n1 -p "Appuyez sur une touche pour continuer..."; echo

#Recompresser
find . | cpio -o -H newc | xz -9 --check=crc32 > "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/${options}/initrd.gz"

cd -
rm -R /tmp/multisystem/multisystem-modinitrd
exit 0
