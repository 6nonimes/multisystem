#!/bin/bash
clear
#Inclure common.sh (fonctions communes)
. ./common.sh

echo -e "\E[37;44m\033[1m ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ \033[0m"
echo -e "\033[1;47;31m Test Debug ! \033[0m"
echo "dossier:${dossier}"
echo -e "\033[1;47;31m ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ \033[0m"
lsb_release -a
echo -e "\033[1;47;31m ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ \033[0m"
sudo dd if=/dev/mem bs=32k skip=30 count=2 | strings | grep -i bios
echo -e "\033[1;47;31m ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ \033[0m"
grub-install -v
echo -e "\033[1;47;31m ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ \033[0m"
uname -a
echo -e "\033[1;47;31m ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ \033[0m"
blkid
echo -e "\033[1;47;31m ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ \033[0m"
cat /proc/cmdline
#ls -l
#./admin.sh
#space-expand="true" space-fill="true"
#homogeneous="true"
