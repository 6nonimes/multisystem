#! /bin/bash
chemin="$(
	cd "$(dirname "$0")"
	pwd
)/$(basename "$0")"
dossier="$(dirname "$chemin")"
export chemin dossier
cd "${dossier}"

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=multisystem
export TEXTDOMAINDIR=${dossier}/locale
. gettext.sh
multisystem=$0

#recup option cdrom
option="$1"

echo -e '\E[37;44m'"\033[1m $(eval_gettext "Qemu nécéssite les droits d\047administrateur") \033[0m"
#
/etc/init.d/kqemu-source force-reload 2>/dev/null

#radiobuttonMultiSystem='Lancer Qemu'
echo -e '\E[37;44m'"\033[1m $(eval_gettext "Démarrer Qemu CLE_USB:")$(cat /tmp/multisystem/multisystem-selection-usb) \033[0m"

if [ $(which apt-get) ]; then
	echo
	if [ ! $(which kvm) ]; then
		echo "$(eval_gettext "installation de qemu")"
		apt-get install -y qemu qemu-kvm kvm-pxe
	fi
fi

#ram dispo?
RAM_LIBRE="$(($(cat /proc/meminfo | grep -e "MemAvailable:" | awk '{print $2}') / 1000))"
#RAM_LIBRE="$(($RAM_LIBRE-50))"
echo "RAM_LIBRE:$RAM_LIBRE MO"

if [ "$RAM_LIBRE" -gt "512" ]; then
	RAM_LIBRE=512
elif [ "$RAM_LIBRE" -gt "256" ]; then
	echo -e '\E[37;44m'"\033[1m $(eval_gettext "Espace ram disponible: ")$RAM_LIBRE Mio \033[0m"
	RAM_LIBRE=256
elif [ "$RAM_LIBRE" -lt "256" ]; then
	echo -e "\033[1;47;31m $(eval_gettext "Erreur: pas assez de ram libre disponible:\$RAM_LIBRE souhaité:256 Mio") \033[0m"
	echo -e "\033[1;47;31m mount -t tmpfs -o size=528m none /dev/shm \033[0m"
	#mount -t tmpfs -o size=528m none /dev/shm
	read
	exit 0
fi
echo "$(eval_gettext "Démarrer qemu pour vérification de boot")"

# Clear filesystem memory cache
# sync
# echo 3 > /proc/sys/vm/drop_caches

#qemu-system-x86_64-spice
if [ $(which qemu-system-x86_64-spice) ]; then
	RUN_QEMU="qemu-system-x86_64-spice -enable-kvm -vga std -no-acpi"
else
	RUN_QEMU="kvm"
fi

#QEMU_AUDIO_DRV=sdl
QEMU_AUDIO_DRV=alsa
if [ ! "${option}" ]; then
	echo N/A

elif [ "${option}" == "usb" ]; then
	echo USB
	#sudo $RUN_QEMU -no-acpi -boot c -usb -usbdevice "disk:$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')" -hda "$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')" -m $RAM_LIBRE 2>/dev/null
	sudo $RUN_QEMU -boot c -usb -hda "$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')" -m $RAM_LIBRE 2>/dev/null

elif [ "${option}" == "cdrom" ]; then
	echo CDROM
	sudo $RUN_QEMU -boot d -cdrom "$HOME/MultiSystem-LiveCD.iso" -m $RAM_LIBRE 2>/dev/null

elif [ "${option}" == "cdamorce" ]; then
	echo CDAMORCE
	sudo $RUN_QEMU -usb -boot d -cdrom "$HOME/cd-boot-liveusb.iso" -hda "$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')" -usbdevice "disk:$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')" -m $RAM_LIBRE 2>/dev/null
fi

#echo RAM_LIBRE:$RAM_LIBRE
#echo Attente
#read

exit 0
#ls /lib/modules/$(uname -r)/kernel/arch/*/kvm/
#sudo modprobe kvm
#dmesg | grep kvm
#lsmod | grep kvm
#-no-kvm
#kvm-ok #commande pour tester si proc compatible avec kvm
