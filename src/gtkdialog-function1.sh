#source ${dossier}/gtkdialog-function1.sh
function FCT_DETECT() {
	#detection clé usb
	echo -n >/tmp/multisystem/multisystem-debog-detect-list
	echo -n >/tmp/multisystem/multisystem-detection-usb
	echo -n >/tmp/multisystem/multisystem-statusbar

	#Détection
	while read line; do
		unset DISK_MOUNTPOINT
		unset DISK_MOUNT
		unset ID_FS_LABEL
		unset ID_FS_LABEL_ENC
		unset ID_FS_UUID
		unset ID_FS_TYPE
		unset ID_FS_VERSION
		unset ID_TYPE
		unset ID_BUS
		unset ID_USB_DRIVER
		unset ID_PART_TABLE_TYPE
		unset DEVNAME
		unset ID_SERIAL
		unset ID_SERIAL_SHORT
		unset UDISKS_PARTITION_NUMBER
		unset DISK_MOUNT
		unset UDISKS_PARTITION_SIZE
		unset ID_VENDOR
		unset ID_MODEL
		#certaines clé USB ne se montrent pas en UDISKS_ mais en DKD_
		unset DKD_PARTITION_NUMBER
		unset DKD_PARTITION_SIZE

		DISK_MOUNT="$(echo ${line} | awk '{print $1}')"
		#Check que est bien monté dans /media ou dans /run/media/$USER (depuis quantal)
		DISK_MOUNTPOINT="$(echo "${line}" | awk '{print $3}' | grep -E "(^/media)|(^/run/media/$USER)")"
		DISK_MOUNT="$(echo "${line}" | awk '{print $1}' | grep ^/dev/)"
		if [ "${DISK_MOUNT}" ]; then
			echo "${DISK_MOUNT}"
			echo "$2"
			eval $(udevadm info -q all -n ${DISK_MOUNT} 2>/dev/null | grep = | grep -E -v ':.*:|\..*=' | awk -F: '{print $2}')
			#████████████████████████████████████████████████████████████████████████████████████████████████████████████
			#████ATTENTION!████ corrige un bug udevadm, selon les versions DEVNAME retourne sdx1 et sur d'autres /dev/sdx1
			#Merci akaoni :)
			DEVNAME=$(basename ${DEVNAME})

			#ATTENTION certaines clé USB ne se montrent pas en UDISKS_ mais en DKD_
			if [ "${DKD_PARTITION_NUMBER}" ]; then
				UDISKS_PARTITION_NUMBER="${DKD_PARTITION_NUMBER}"
			fi
			if [ "${DKD_PARTITION_SIZE}" ]; then
				UDISKS_PARTITION_SIZE="${DKD_PARTITION_SIZE}"
			fi

			#Special Lubuntu 12.10
			if [ ! "${UDISKS_PARTITION_NUMBER}" ]; then
				UDISKS_PARTITION_NUMBER="${ID_PART_ENTRY_NUMBER}"
			fi
			if [ ! "${UDISKS_PARTITION_SIZE}" ]; then
				UDISKS_PARTITION_SIZE="${ID_PART_ENTRY_SIZE}"
			fi

			#████████████████████████████████████████████████████████████████████████████████████████████████████████████
			#Debug
			echo -e "line:${line}
DISK_MOUNT:${DISK_MOUNT}
DISK_MOUNTPOINT:${DISK_MOUNTPOINT}
ID_FS_LABEL:${ID_FS_LABEL}
ID_FS_LABEL_ENC:${ID_FS_LABEL_ENC}
ID_FS_UUID:${ID_FS_UUID}
ID_FS_TYPE:${ID_FS_TYPE}
ID_FS_VERSION:${ID_FS_VERSION}
ID_TYPE:${ID_TYPE}
ID_BUS:${ID_BUS}
ID_USB_DRIVER:${ID_USB_DRIVER}
ID_PART_TABLE_TYPE:${ID_PART_TABLE_TYPE}
DEVNAME:${DEVNAME}
ID_SERIAL:${ID_SERIAL}
ID_SERIAL_SHORT:${ID_SERIAL_SHORT}
UDISKS_PARTITION_NUMBER:${UDISKS_PARTITION_NUMBER}
DISK_MOUNT:${DISK_MOUNT}
UDISKS_PARTITION_SIZE:${UDISKS_PARTITION_SIZE}
ID_VENDOR:${ID_VENDOR}
ID_MODEL:${ID_MODEL} \n" >>/tmp/multisystem/multisystem-debog-detect-list
			#Limiter aux disques montés dans /media
			if [ "$(grep -E "(^/media)|(^/run/media/$USER)" <<<"${DISK_MOUNTPOINT}")" ]; then
				echo
				#Verif que device dans mount et udevadm sont identique
				if [ "${DISK_MOUNT}" = "/dev/${DEVNAME}" ]; then
					echo
					#Verif type (disk)|(sd_mmc)|(floppy)
					if [ "$(echo "${ID_TYPE}" | grep -E "(disk)|(sd_mmc)|(floppy)")" ]; then
						echo
						#Verif bus (usb)|(mmc)
						if [ "$(echo "${ID_BUS}" | grep -E "(usb)|(mmc)|(ata)")" ]; then
							echo
							#Verif FAT32
							if [ "${ID_FS_VERSION}" = "FAT32" ]; then
								echo
								#Verif que est bien la première partition
								if [ "${UDISKS_PARTITION_NUMBER}" = "1" ]; then
									echo
									#Verif que releve bien UUID
									if [ "${ID_FS_UUID}" ]; then
										#DEVICE|VENDOR|MODEL|DISK_SIZE|DISK_AVAILABLE|DISK_USE|TYPE|DRIVER|MOUNTPOINT|UUID
										echo "${DISK_MOUNT}|${ID_VENDOR}|${ID_MODEL}|$(($(/bin/df -P 2>/dev/null | grep -w ^${DISK_MOUNT} | awk '{print $2}') / 1024)) Mio|$(($(/bin/df -P 2>/dev/null | grep -w ^${DISK_MOUNT} | awk '{print $3}') / 1024)) Mio|$(($(/bin/df -P 2>/dev/null | grep -w ^${DISK_MOUNT} | awk '{print $4}') / 1024)) Mio|${ID_TYPE}|${ID_BUS}|${DISK_MOUNTPOINT}|${ID_FS_UUID}" >>/tmp/multisystem/multisystem-detection-usb
									fi
								fi
							fi
						fi
					fi
				fi
			fi
		fi
	done <<<"$(mount | grep vfat)"
}
export -f FCT_DETECT

#Lister les lang
function FCT_lister_lang() {
	cat "$HOME/.multisystem/lang_sel.txt" | awk -F'|' '{print $1}'
	cat "${dossier}/../lang_list.txt" | sed "/^$/d" | awk -F'|' '{print $1}'
}
export -f FCT_lister_lang

#Icon thème
#cp -f "./pixmaps/multisystem-$(cat "${HOME}"/.multisystem-theme).png" /tmp/multisystem/multisystem-theme.png

#Message statusbar
function FCT_statusbar() {
	echo device:${device}
	if [ ! "$(cat /tmp/multisystem/multisystem-detection-usb 2>/dev/null)" ]; then
		echo -e "$(eval_gettext "Avez-vous branché votre clé USB ?\n\nVeuillez connecter un volume usb\nformaté en fat32\net le monter dans /media\n\nPuis relancez MultiSystem.")" >/tmp/multisystem/multisystem-statusbar
	elif [ "${device}" ]; then
		echo "Device:${device}" >/tmp/multisystem/multisystem-statusbar
	else
		rm /tmp/multisystem/multisystem-statusbar
	fi
}
export -f FCT_statusbar
