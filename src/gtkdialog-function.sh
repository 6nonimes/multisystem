#! /bin/bash --posix
chemin="$(
	cd "$(dirname "$0")"
	pwd
)/$(basename "$0")"
dossier="$(dirname "$chemin")"
export chemin dossier
cd "${dossier}"

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export -p TEXTDOMAIN=multisystem
export -p TEXTDOMAINDIR=${dossier}/locale
. gettext.sh
multisystem=$0

#source ${dossier}/gtkdialog-function.sh

#Onglet Terminal vte
#Fonction vte
#https://gtkdialog.googlecode.com/svn/trunk/examples/terminal/terminal
function FCT_vte() {
	echo -n >/tmp/multisystem/multisystem-vte
	echo -n >/tmp/multisystem/multisystem-option1
	echo -n >/tmp/multisystem/multisystem-option2
	echo -n >/tmp/multisystem/multisystem-option3
	fileselect="$(cat /tmp/multisystem/multisystem-fileselect 2>/dev/null)"
	fileselect="$(echo -e "${fileselect//\%/\\x}" | sed 's%^file://%%' | sed 's#\r##g')"
	export fileselect
	echo -e "${fileselect}" >/tmp/multisystem/multisystem-fileselect

	#add
	if [[ "$1" = "addvte" && "${fileselect}" && -f "${fileselect}" ]]; then
		cp -f /tmp/multisystem/multisystem-function_name /tmp/multisystem/multisystem-option1
		cp -f /tmp/multisystem/multisystem-fileselect /tmp/multisystem/multisystem-option2
		>/tmp/multisystem/multisystem-option3
		cp -f ./iso_add.sh /tmp/multisystem/multisystem-vte

	#addvtemulti
	elif [ "$1" = "addvtemulti" ]; then
		echo "addvtemulti" >/tmp/multisystem/multisystem-option3
		cp -f ./iso_addmulti.sh /tmp/multisystem/multisystem-vte

	#selclear
	elif [[ "$1" = "selclear" && "${fileselect}" ]]; then
		cp -f /tmp/multisystem/multisystem-function_name /tmp/multisystem/multisystem-option1
		cp -f /tmp/multisystem/multisystem-fileselect /tmp/multisystem/multisystem-option2
		>/tmp/multisystem/multisystem-option3
		cp -f ./iso_remove.sh /tmp/multisystem/multisystem-vte

	#selup
	elif [ "$1" = "selup" ]; then
		cp -f /tmp/multisystem/multisystem-function_name /tmp/multisystem/multisystem-option1
		cp -f /tmp/multisystem/multisystem-fileselect /tmp/multisystem/multisystem-option2
		>/tmp/multisystem/multisystem-option3
		cp -f ./iso_selup.sh /tmp/multisystem/multisystem-vte

	#seldown
	elif [ "$1" = "seldown" ]; then
		cp -f /tmp/multisystem/multisystem-function_name /tmp/multisystem/multisystem-option1
		cp -f /tmp/multisystem/multisystem-fileselect /tmp/multisystem/multisystem-option2
		>/tmp/multisystem/multisystem-option3
		cp -f ./iso_seldown.sh /tmp/multisystem/multisystem-vte

	#move
	elif [ "$1" = "move" ]; then
		cp -f /tmp/multisystem/multisystem-function_name /tmp/multisystem/multisystem-option1
		cp -f /tmp/multisystem/multisystem-fileselect /tmp/multisystem/multisystem-option2
		>/tmp/multisystem/multisystem-option3
		cp -f ./iso_move.sh /tmp/multisystem/multisystem-vte

	#grub
	elif [ "$1" = "grub" ]; then
		>/tmp/multisystem/multisystem-option1
		>/tmp/multisystem/multisystem-option2
		>/tmp/multisystem/multisystem-option3
		cp -f ./grub.sh /tmp/multisystem/multisystem-vte

	#cdamorce
	elif [ "$1" = "cdamorce" ]; then
		>/tmp/multisystem/multisystem-option1
		>/tmp/multisystem/multisystem-option2
		>/tmp/multisystem/multisystem-option3
		cp -f ./cdamorce.sh /tmp/multisystem/multisystem-vte

	#debugvte
	elif [ "$1" = "debugvte" ]; then
		cp -f ./debug.sh /tmp/multisystem/multisystem-vte

	else
		echo -e "echo N/A
echo 0 >/tmp/multisystem/multisystem-inputtab1
echo 0 >/tmp/multisystem/multisystem-inputtab2
echo 0 >/tmp/multisystem/multisystem-inputtab3
echo "" >/tmp/multisystem/multisystem-fileselect
echo false >/tmp/multisystem/multisystem-timer
" >/tmp/multisystem/multisystem-vte

	fi

	echo . "/tmp/multisystem/multisystem-vte"
}
export -f FCT_vte

#Onglet lister_lang
function FCT_lister_lang() {
	cat "$HOME/.multisystem/lang_sel.txt" | awk -F'|' '{print $1}'
	cat "${dossier}/../lang_list.txt" | sed "/^$/d" | awk -F'|' '{print $1}'
}
export -f FCT_lister_lang

#Onglet Télécharger des LiveCD
function FCT_download_livecd() {
	listcat="$(cat /tmp/multisystem/multisystem-output-list 2>/dev/null)"
	#Audio Utility Antivirus Gamer
	if [ "$(grep -E "(Audio)|(Utility)|(Antivirus)|(Gamer)" <<<"${listcat}" 2>/dev/null)" ]; then
		cat "${dossier}/../list.txt" | sed "/^#/d" | sed "/^$/d" | grep -E "\|${listcat}\|"
	elif [ "${listcat}" = "All" ]; then
		cat "${dossier}/../list.txt" | grep -v "^#" | sed "/^$/d"
	elif [ "${listcat}" = "clear" ]; then
		echo "||||"
	elif [ "${listcat}" ]; then
		cat "${dossier}/../list.txt" | sed "/^#/d" | sed "/^$/d" | grep -iE "\|.*${listcat}.*\|.*\|.*\|"
	fi
}
export -f FCT_download_livecd

#Onglet Mise à jour
function FCT_update() {
	$radio1 && xdg-open 'http:\\liveusb.info/dotclear/index.php?pages/Soutien' &
	if [ "$radio3" == "true" ]; then
		nohup ./update-sel.sh &
	#maj partielle
	elif [ "$radio4" == "true" ]; then
		wget -nd http:\\liveusb.info/multisystem/os_support.sh -O /tmp/multisystem/os_support.sh 2>&1 |
			sed -u 's/\([ 0-9]\+K\)[ \.]*\([0-9]\+%\) \(.*\)/\2\n#Transfert : \1 (\2) à \3/' |
			zenity --progress --auto-kill --auto-close --width 400 --title "$(eval_gettext 'Téléchargement en cours...')"
		if [ "$(diff /tmp/multisystem/os_support.sh ${dossier}/os_support.sh 2>/dev/null)" ]; then
			#Remplacer...
			echo
			if [ "$(du -h "/tmp/multisystem/os_support.sh" 2>/dev/null | awk '{print $1}')" == "0" ]; then
				zenity --error --text "$(eval_gettext 'Erreur de téléchargement')"
			elif [ "$(grep FCT_RELOAD /tmp/multisystem/os_support.sh 2>/dev/null)" ]; then
				cp -f /tmp/multisystem/os_support.sh "${dossier}/os_support.sh"
			fi
		else
			zenity --info --title MultiSystem_Information --text="$(eval_gettext "Pas de mise à jour disponible,\nVous utilisez bien la dernière version du script.")"
		fi
		#Relancer gui
		nohup "${dossier}/gui_multisystem.sh" &
		sleep 1
		exit 0
	fi
}
export -f FCT_update

#test écriture sur disque
function FCT_debug_write() {
	#espace dispo dans clé usb
	available="$(($(df -aB 1 $(cat /tmp/multisystem/multisystem-mountpoint-usb) 2>/dev/null | grep ^$(cat /tmp/multisystem/multisystem-selection-usb) | awk '{print $4}') / 1024 / 1024))"
	if [ "${available}" -lt "1024" ]; then
		message_debug="$(eval_gettext "Erreur: pas suffisament de place libre pour effectuer ce test,") ${available} < 1024Mio"
		echo "${message_debug}" >/tmp/multisystem/multisystem-test-usb
		exit 0
	fi
	echo -e "\E[37;44m\033[1m ${message_debug} \033[0m"
	dd if=/dev/zero bs=1024 count=1000000 of="$(cat /tmp/multisystem/multisystem-mountpoint-usb)/multisystem-1Gb.file"
	rm "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/multisystem-1Gb.file"
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Attente, appuyez sur enter pour continuer.') \033[0m"
	read
	echo "" >/tmp/multisystem/multisystem-test-usb
}

#test lecture
function FCT_debug_read() {
	#dd if=/dev/zero bs=1024 count=1000000 of="$(cat /tmp/multisystem/multisystem-mountpoint-usb)/multisystem-1Gb.file" >dev/null
	#dd if="$(cat /tmp/multisystem/multisystem-mountpoint-usb)/multisystem-1Gb.file" bs=64k | dd of=/dev/null
	#rm "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/multisystem-1Gb.file"
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Veuillez saisir votre mot de passe d\047administrateur') \033[0m"
	sudo hdparm -Tt $(cat /tmp/multisystem/multisystem-selection-usb)
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Attente, appuyez sur enter pour continuer.') \033[0m"
	read
}

#reparer
function FCT_debug_repair() {
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Veuillez saisir votre mot de passe d\047administrateur') \033[0m"
	#redimensionner
	sudo umount -f $(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')1 2>/dev/null
	sleep 2
	sudo dosfsck -r -w -v $(cat /tmp/multisystem/multisystem-selection-usb)
	calc_fatresize="$(sudo fatresize -i $(cat /tmp/multisystem/multisystem-selection-usb) -q | grep '^Max size' | awk '{print $3}')"
	echo calc_fatresize:${calc_fatresize} $((${calc_fatresize} / 1000 / 1000))M
	sudo fatresize -p -s $((${calc_fatresize} / 1000 / 1000))M $(cat /tmp/multisystem/multisystem-selection-usb)
	#gvfs-mount -d $(cat /tmp/multisystem/multisystem-selection-usb) 2>/dev/null
	#
	#Remplacement de gvfs-mount par "libglib2.0-bin" ==> gio
	gio mount -d $(cat /tmp/multisystem/multisystem-selection-usb) 2>/dev/null
	#
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Attente, appuyez sur enter pour continuer.') \033[0m"
	read
}
export -f FCT_debug_write FCT_debug_read FCT_debug_repair

#Onglet N°5 Formater votre clé USB
function FCT_format() {
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Veuillez saisir votre mot de passe d\047administrateur') \033[0m"
	sudo echo
	#Démonter
	sudo umount -f $(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')1 2>/dev/null
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Veuillez saisir votre mot de passe d\047administrateur') \033[0m"
	#shred  - Écrire par dessus un fichier pour en camoufler le contenu, et optionnellement l’effacer
	#shred -n 1 -z -v $(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')
	sudo dd if=/dev/zero of=$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//') bs=512 count=1
	sudo parted -a cylinder -s $(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//') mklabel msdos
	sudo parted -a cylinder -s $(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//') unit MB mkpart primary fat32 1 100%
	sleep 2
	sudo umount -f $(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')1 2>/dev/null
	sudo mkdosfs -F32 -v -S512 -n multisystem $(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')1
	sleep 2
	sudo parted -s $(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//') set 1 boot on
	#Vérifier/Réparer fat32
	#sudo dosfsck -t -a -r -v $(cat /tmp/multisystem/multisystem-selection-usb)
	sudo dosfsck -p -a -w -v $(cat /tmp/multisystem/multisystem-selection-usb)
	sleep 2
	sudo umount $(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')1 2>/dev/null
	#redimensionner
	#calc_fatresize="$(sudo fatresize -i $(cat /tmp/multisystem/multisystem-selection-usb) | grep '^Max size' | awk '{print $3}')"
	#sudo fatresize -p -s ${calc_fatresize} $(cat /tmp/multisystem/multisystem-selection-usb)
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Formatage effectué,\nveuillez débrancher/rebrancher votre clé USB\navant de relancer multisystem.') \033[0m"
	read
	exit 0
}
export -f FCT_format

#Onglet N°6 Afficher/Masquer des fichiers/dossiers dans votre clé usb
function FCT_hidden_update_tree() {
	#Mettre en forme pour le tree
	>/tmp/multisystem/multisystem-hidden
	echo -e "$(stat -c %n "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/"* | awk '{print $0}')" | while read line; do
		var="$(echo "$line" | sed "s|$(cat /tmp/multisystem/multisystem-mountpoint-usb)/||")"
		echo "var:${var}"
		if [ "$(grep "^${var}$" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden")" ]; then
			echo "multisystem-red|$(cat /tmp/multisystem/multisystem-mountpoint-usb)/${var}" >>/tmp/multisystem/multisystem-hidden
		else
			echo "multisystem-green|$(cat /tmp/multisystem/multisystem-mountpoint-usb)/${var}" >>/tmp/multisystem/multisystem-hidden
		fi
	done
}

function FCT_hidden_showall() {
	stat -c %n "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/"* | awk '{print "multisystem-green|" $0}' >/tmp/multisystem/multisystem-hidden
	echo "" >"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden"
	FCT_hidden_update_tree
}

function FCT_hidden_hiddenall() {
	stat -c %n "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/"* | awk '{print "multisystem-red|" $0}' >/tmp/multisystem/multisystem-hidden
	echo -e "$(stat -c %n "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/"* | awk '{print $0}')" | sed "s|$(cat /tmp/multisystem/multisystem-mountpoint-usb)/||" >"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden"
	FCT_hidden_update_tree
}

function FCT_hidden_modify() {
	echo
	rechercher="$(echo "${1}" | sed "s|$(cat /tmp/multisystem/multisystem-mountpoint-usb)/||")"
	if [ ! "$(grep "^${rechercher}$" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden")" ]; then
		#zenity --info --text "Masquer ${rechercher}"
		echo "${rechercher}" >>"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden"
	else
		#zenity --info --text "Afficher ${rechercher}"
		sed -i "s|^${rechercher}$||" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden"
		sed -i "/^$/d" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden"
	fi
	FCT_hidden_update_tree
}

export -f FCT_hidden_showall FCT_hidden_hiddenall FCT_hidden_modify FCT_hidden_update_tree

function comment() { true; }
export -f comment
