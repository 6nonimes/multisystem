#sortie des fonctions pour Ubuntu 16.04 !!!
#source ${dossier}/gtkdialog-function2.sh

#Supprimer sélection user
function FCT_multi_sel_delsel() {
	sed -i "s@gtk-cdrom|${*}.*@@g" /tmp/multisystem/multisystem-sel-multi
	sed -i "/^$/d" /tmp/multisystem/multisystem-sel-multi
}

#Ajouter sélection user
function FCT_multi_sel_addsel() {
	if [ "$(cat /tmp/multisystem/multisystem-sel-multi | grep "${*}")" ]; then
		wmctrl -c "$(eval_gettext 'Erreur: LiveCD déjà présent.')"
		zenity --title="$(eval_gettext 'Erreur: LiveCD déjà présent.')" --error --text "$(eval_gettext 'Erreur: LiveCD déjà présent.')" &
		sleep 1
		wmctrl -a "$(eval_gettext 'Erreur: LiveCD déjà présent.')"
	else
		#verifier si nom fichier ne pose pas problemes
		if [ ! -f "${*}" ]; then
			zenity --error --text "$(eval_gettext "Erreur:Le fichier iso sélectionné contiens un caractère non supporté dans son nom:")"
		fi
		if [ "$(echo "$(basename "${*}")" | grep -iE "(\.iso$)|(\.img$)")" ]; then
			echo "gtk-cdrom|${*}|$(($(du -sB 1 "${*}" | awk '{print $1}') / 1024 / 1024))" >>/tmp/multisystem/multisystem-sel-multi
		fi
	fi
}

#Calcul taille restante
function FCT_multi_sel_calcsize() {
	echo $(($(df -B 1 | grep ^$(cat /tmp/multisystem/multisystem-selection-usb) | awk '{print $4}') / 1024 / 1024)) >/tmp/multisystem/multisystem-sel-calcsize
	total="0"
	cat /tmp/multisystem/multisystem-sel-multi | awk -F '|' '{print $2}' | while read line; do
		tiso="$(($(du -sB 1 "$line" | awk '{print $1}') / 1024 / 1024))"
		total="$(($total + $tiso))"
		#echo $total
		echo $(($(($(df -B 1 | grep ^$(cat /tmp/multisystem/multisystem-selection-usb) | awk '{print $4}') / 1024 / 1024)) - $total)) >/tmp/multisystem/multisystem-sel-calcsize
	done
	#verifier espace disponible #>=
	if [ "10" -ge "$(cat /tmp/multisystem/multisystem-sel-calcsize)" ]; then
		zenity --error --text "$(eval_gettext "Erreur: pas suffisament d\047espace libre:")" &
		delsel="$(awk 'END {print}' /tmp/multisystem/multisystem-sel-multi)"
		sed -i "s@${delsel}@@" /tmp/multisystem/multisystem-sel-multi
		sed -i "/^$/d" /tmp/multisystem/multisystem-sel-multi
		#recalculer la taille
		./gui_multi_sel.sh calcsize
	fi
}

export -f FCT_multi_sel_delsel FCT_multi_sel_addsel FCT_multi_sel_calcsize
