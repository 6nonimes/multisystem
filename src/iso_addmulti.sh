clear
#echo -e "\E[37;44m\033[1m ooo \033[0m"
#echo -e "\033[1;47;31m ooo \033[0m"

#Inclure common.sh (fonctions communes)
#export -f FCT_ERROR
#export -f FCT_END
#export -f FCT_KILL
#export -f FCT_UPDATEGRUB
#export -f FCT_ADDCOMMON
#export -f FCT_UPDATETREE
. ./common.sh

#monter gui
I=$IFS
IFS=""
for MENU_INFO in "$(./gui_multi_sel.sh)"; do
	eval $MENU_INFO
done
IFS=$I

if [ "$EXIT" != "ok" ]; then
	>/tmp/multisystem/multisystem-sel-multi
fi

i=0
while read line; do
	let i++
	echo addvtemulti >/tmp/multisystem/multisystem-option1
	echo "$line" >/tmp/multisystem/multisystem-option2
	echo "${i}" >/tmp/multisystem/multisystem-option3
	echo "add $line ${i}"
	. ./iso_add.sh
done <<<"$(cat /tmp/multisystem/multisystem-sel-multi | awk -F '|' '{print $2}')"

#Mettre à jour les bootloader
if [ "$(cat /tmp/multisystem/multisystem-update-bootloader)" = "true" ]; then
	FCT_UPDATEGRUB
fi

FCT_UPDATETREE
FCT_END
exit 0
