#echo -e "\E[37;44m\033[1m ooo \033[0m"
#echo -e "\033[1;47;31m ooo \033[0m"

echo fileselect:$(cat /tmp/multisystem/multisystem-fileselect)
echo function_name:$(cat /tmp/multisystem/multisystem-function_name)
echo -e "\E[37;44m\033[1m option1:$(cat /tmp/multisystem/multisystem-option1) \033[0m"
echo -e "\E[37;44m\033[1m option2:$(cat /tmp/multisystem/multisystem-option2) \033[0m"
echo -e "\E[37;44m\033[1m option3:$(cat /tmp/multisystem/multisystem-option3) \033[0m"

function FCT_STOPTIMER() { #stoppe timer et retour Accueil
	echo -n >/tmp/multisystem/multisystem-option1
	echo -n >/tmp/multisystem/multisystem-option2
	echo -n >/tmp/multisystem/multisystem-option3
	echo 0 >/tmp/multisystem/multisystem-inputtab1
	echo 0 >/tmp/multisystem/multisystem-inputtab2
	echo 0 >/tmp/multisystem/multisystem-inputtab3
	echo "" >/tmp/multisystem/multisystem-fileselect
	echo "" >/tmp/multisystem/multisystem-function_name
	echo false >/tmp/multisystem/multisystem-timer
}

function FCT_ERROR() {
	sync
	sudo umount "/tmp/multisystem/multisystem-mountpoint-iso" 2>/dev/null
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Attente, appuyez sur enter pour continuer.') \033[0m"
	read
	#▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
	FCT_STOPTIMER
}

function FCT_END() {
	#echo -e "\E[37;44m\033[1m Attente Debug \033[0m"
	#read
	sync
	sudo umount "/tmp/multisystem/multisystem-mountpoint-iso" 2>/dev/null
	#▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
	FCT_STOPTIMER
}

function FCT_KILL() {
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Attente, appuyez sur enter pour continuer.') \033[0m"
	read
	nohup ./kill.sh &
	exit 0
}

function FCT_UPDATEGRUB() {
	. ./update_grub.sh
}

function FCT_ADDCOMMON() {
	#Remplacer multiboot par multisystem ! dans la clé usb branchée.
	if [ "$(grep '#MULTIBOOT' "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null)" ]; then
		#Remplacer MULTIBOOT
		#grub2
		sed -i "s/\#MULTIBOOT/\#MULTISYSTEM/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null
		sed -i "s/\#MULTIBOOT/\#MULTISYSTEM/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/menu.lst" 2>/dev/null
		#Burg
		sed -i "s/\#MULTIBOOT/\#MULTISYSTEM/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/burg/menu.txt" 2>/dev/null
		sed -i "s/\#MULTIBOOT/\#MULTISYSTEM/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/burg/burg.cfg" 2>/dev/null
		#Syslinux
		sed -i "s/\#MULTIBOOT/\#MULTISYSTEM/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/syslinux.cfg" 2>/dev/null
		#Remplacer multiboot-v3-
		#grub2
		sed -i "s/multiboot-v3-/multisystem-/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null
		sed -i "s/multiboot-v3-/multisystem-/" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/menu.lst" 2>/dev/null
		#Burg
		sed -i "s/multiboot-v3-/multisystem-/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/burg/menu.txt" 2>/dev/null
		sed -i "s/multiboot-v3-/multisystem-/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/burg/burg.cfg" 2>/dev/null
		#Syslinux
		sed -i "s/multiboot-v3-/multisystem-/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/syslinux.cfg" 2>/dev/null
		sleep 1
		#Supprimer multiboot.bat
		if [ -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/multiboot.bat" ]; then
			rm "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/multiboot.bat"
		fi
		#Renommer dans .hidden
		if [ -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden" ]; then
			sed -i "s/multiboot.bat/multisystem.bat/g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden"
		fi
		#Renommer sauvegarde du mbr
		if [ -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/multiboot.bs.save" ]; then
			mv "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/multiboot.bs.save" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/multisystem.bs.save"
		fi
		FCT_UPDATEGRUB
	fi

	#Vérifier que la clé USB est bien toujours présente!
	echo -n >/tmp/multisystem/multisystem-laisserpasser2-usb
	unset ID_FS_UUID
	unset ID_FS_LABEL
	#Old --- eval $(udevadm info -q all -n $(cat /tmp/multisystem/multisystem-selection-usb 2>/dev/null) 2>/dev/null | grep = | grep -v ':.*:' | awk -F: '{print $2}')
	eval $(udevadm info -q all -n $(cat /tmp/multisystem/multisystem-selection-usb 2>/dev/null) 2>/dev/null | grep = | grep -v '\..*=' | grep -v ':.*:' | awk -F: '{print $2}')

	#Verif mountpoint
	if [ ! -d "$(cat /tmp/multisystem/multisystem-mountpoint-usb)" ]; then
		echo -e "\033[1;47;31m $(eval_gettext "Erreur:le volume suivant est indisponible:")$(cat /tmp/multisystem/multisystem-selection-usb) \033[0m"
	#Verif UUID
	elif [ "$(cat /tmp/multisystem/multisystem-selection-uuid-usb)" != "${ID_FS_UUID}" ]; then
		echo -e "\033[1;47;31m $(eval_gettext "Erreur: La clé USB n\047est plus la même!") \033[0m"
	#ok
	else
		echo ok >/tmp/multisystem/multisystem-laisserpasser2-usb
	fi
	#Exit si pas ok
	if [ "$(cat /tmp/multisystem/multisystem-laisserpasser2-usb)" != "ok" ]; then
		echo -e "\033[1;47;31m $(eval_gettext "Erreur:le volume suivant est indisponible:")$(cat /tmp/multisystem/multisystem-selection-usb) \033[0m"
		rm -R /tmp/multisystem 2>/dev/null
		FCT_KILL
		exit 0
	fi

	#copier dossier boot si pas present
	if [[ ! -f "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")"/boot/grub/grub.cfg || ! -f "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")"/boot/grub/menu.lst ]]; then
		cp -Rf "${dossier}/boot" "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")"/
		if [ "$(grub-install -v | grep 1.96)" ]; then
			echo
			if [ -f "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")/boot/grub/grub-v1.96.cfg" ]; then
				rm "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")/boot/grub/grub.cfg"
				mv "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")/boot/grub/grub-v1.96.cfg" "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")/boot/grub/grub.cfg"
			fi
		else
			rm "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")/boot/grub/grub-v1.96.cfg"
		fi
	fi

	#rajouter PloP dans clé si pas présent!
	if [ -f "${HOME}"/.multisystem/nonfree/plpbt.bin ]; then
		echo ok plpbt.bin
		if [ ! -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/img/plpbt" ]; then
			cp -Rf "${HOME}"/.multisystem/nonfree/plpbt.bin "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")/boot/img/plpbt"
		fi
	fi
	#copier dossier syslinux si pas present, car ajouté tardivement et user peut ne pas avoir.
	if [ ! -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/syslinux.cfg" ]; then
		cp -Rf "${dossier}/boot/syslinux" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/"
		rm "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/syslinux"
	fi
	#Ajouter un dossier .hidden pour masques fichiers/dossiers.
	if [ ! -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden" ]; then
		echo -e "#boot
        autorun.inf
        icon.ico
        multisystem.bat" | tee "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.hidden"
	fi
	#verifier presence marqueurs
	if [ "$(grep "#MULTISYSTEM_ST" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" | wc -l)" != "2" ]; then
		echo -e "\033[1;47;31m $(eval_gettext "Erreurs: fichier de configuration grub.cfg non conforme") \033[0m"
		FCT_KILL
		exit 0
	elif [ "$(grep "#MULTISYSTEM_ST" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/menu.lst" | wc -l)" != "2" ]; then
		echo -e "\033[1;47;31m $(eval_gettext "Erreurs: fichier de configuration menu.lst non conforme") \033[0m"
		FCT_KILL
		exit 0
	elif [ "$(grep "#MULTISYSTEM_ST" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/syslinux.cfg" | wc -l)" != "2" ]; then
		echo -e "\033[1;47;31m $(eval_gettext "Erreurs: fichier de configuration syslinux.cfg non conforme") \033[0m"
		FCT_KILL
		exit 0
	fi
	#Conpter le nonbre de fichier iso present
	echo "$(grep '^multisystem-' "/tmp/multisystem/multisystem-mise-en-forme" 2>/dev/null | wc -l)" >/tmp/multisystem/multisystem-nombreiso-usb
}

function FCT_UPDATETREE() {
	#mettre à jour le contenu du tree
	#relever icone|iso|date
	echo -n >/tmp/multisystem/multisystem-mise-en-forme
	listetree="$(sed -n '/^#MULTISYSTEM_MENU_DEBUT/p' "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")/boot/grub/grub.cfg" | awk -F"|" '{print $4 "|" $3 "|" $2 "|" $5 "|" $6 "|Grub2"}')"
	if [ "${listetree}" ]; then
		echo -e "$listetree" >/tmp/multisystem/multisystem-mise-en-forme
	fi
	listetree2="$(sed -n '/^#MULTISYSTEM_MENU_DEBUT/p' "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")/boot/grub/menu.lst" | awk -F"|" '{print $4 "|" $3 "|" $2 "|" $5 "|" $6 "|Grub4dos"}')"
	if [ "${listetree2}" ]; then
		echo -e "$listetree2" >>/tmp/multisystem/multisystem-mise-en-forme
	fi
	listetree3="$(sed -n '/^#MULTISYSTEM_MENU_DEBUT/p' "$(cat "/tmp/multisystem/multisystem-mountpoint-usb")/boot/syslinux/syslinux.cfg" | awk -F"|" '{print $4 "|" $3 "|" $2 "|" $5 "|" $6 "|Syslinux"}')"
	if [ "${listetree3}" ]; then
		echo -e "$listetree3" >>/tmp/multisystem/multisystem-mise-en-forme
	fi
	#Conpter le nonbre de fichier iso present
	echo "$(grep '^multisystem-' "/tmp/multisystem/multisystem-mise-en-forme" | wc -l)" >/tmp/multisystem/multisystem-nombreiso-usb
}

export -f FCT_STOPTIMER
export -f FCT_ERROR
export -f FCT_END
export -f FCT_KILL
export -f FCT_UPDATEGRUB
export -f FCT_ADDCOMMON
export -f FCT_UPDATETREE
FCT_ADDCOMMON
