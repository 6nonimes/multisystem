#! /bin/bash
chemin="$(
	cd "$(dirname "$0")"
	pwd
)/$(basename "$0")"
dossier="$(dirname "$chemin")"
export chemin dossier
cd "${dossier}"

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=multisystem
export TEXTDOMAINDIR=${dossier}/locale
. gettext.sh
multisystem=$0

#verifier Install VirtualBox
if [ ! "$(which VBoxManage)" ]; then
	xterm -title 'install VBox' -e './vbox_install.sh'
fi

#re-verifier Install VirtualBox
if [ ! "$(which VBoxManage)" ]; then
	echo "Erreur1 VBoxManage"
	exit 0
fi

#Thème
. ./theme.sh

#Au cas ou lance directement par son menu...
mkdir /tmp/multisystem 2>/dev/null

#
export MOD_WAIT2='<window title="MultiSystem-logo2" window_position="0" decorated="false">
<vbox>
<pixmap>
<input file>../img/logo.png</input>
</pixmap>
<pixmap>
<input file>../pixmaps/multisystem-wait.gif</input>
</pixmap>
</vbox>
</window>'
gtkdialog --program=MOD_WAIT2 &

#sortie des fonctions pour Ubuntu 16.04 !!!
source ${dossier}/gtkdialog-function5.sh

#Copier "${theme_bdo}" dans /tmp/multisystem/multisystem-load.gif
cp -f "${theme_bdo}" /tmp/multisystem/multisystem-load.gif

sleep .5
wmctrl -c "MultiSystem-logo2"

DROPISO='<window width_request="400" window_position="0" title="VBox" icon-name="multisystem-icon" decorated="true" resizable="false">
<vbox spacing="0">

<pixmap>
<input file>'${theme_bdo}'</input>
</pixmap>

<comboboxtext>
<variable>ostypes</variable>
<input>bash -c "source ${dossier}/gtkdialog-function5.sh;FCT_listtypes"</input>
</comboboxtext>

<pixmap>
<input file>'${theme_bdo}'</input>
</pixmap>

<hbox>
<frame '$(eval_gettext 'Glisser/Déposer iso/img')'>
<hbox>

<entry 
accept="filename" activates-default="true" fs-folder="'$HOME/'" fs-action="file" 
fs-filters="*.iso" show-hidden="false" fs-title="Select an iso file"

primary-icon-name="'${theme_btn}'" 
secondary-icon-name="multisystem-virtualbox" 

tooltip-text="'$(eval_gettext "Glisser/Déposer iso/img")'\n'$(eval_gettext 'Ajouter un liveCD')'"
primary-icon-tooltip-text="'$(eval_gettext 'Utilisez ce bouton si le Glisser/Déposer ne fonctionne pas.')'"
secondary-icon-tooltip-text="'$(eval_gettext 'Ouvrir VirtualBox')'">

<variable>DAG2</variable>
<width>50</width><height>50</height>
<action signal="changed">test $DAG2 && bash -c "source ${dossier}/gtkdialog-function5.sh;FCT_livecd" &</action>
<action signal="changed">clear:DAG2</action>
<action signal="changed">refresh:DAG2</action>

<action signal="primary-icon-press">fileselect:DAG2</action>
<action signal="secondary-icon-press">VirtualBox &</action>
</entry>

</hbox>
</frame>

</hbox>

<vbox>
<pixmap>
<variable>multisystem-wait</variable>
<input file>/tmp/multisystem/multisystem-load.gif</input>
</pixmap>

<timer milliseconds="true" interval="500" visible="false">
<variable>TIMER</variable>
<sensitive>true</sensitive>
<action>refresh:multisystem-wait</action>
</timer>
</vbox>

</vbox>
</window>'
export DROPISO
gtkdialog -p DROPISO
exit 0
