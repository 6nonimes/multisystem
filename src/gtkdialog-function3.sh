#sortie des fonctions pour Ubuntu 16.04 !!!
#source ${dossier}/gtkdialog-function3.sh

#texteditor
function FCT_texteditor() {
	texteditor="gedit"
	if which gedit >/dev/null 2>&1; then
		texteditor="gedit"
	elif which kwrite >/dev/null 2>&1; then
		texteditor="kwrite"
	elif which kate >/dev/null 2>&1; then
		texteditor="kate"
	elif which geany >/dev/null 2>&1; then
		texteditor="geany"
	elif which pluma >/dev/null 2>&1; then
		texteditor="pluma"
	elif which scratch-text-editor >/dev/null 2>&1; then
		texteditor="scratch-text-editor"
	elif which leafpad >/dev/null 2>&1; then
		texteditor="leafpad"
	elif which mousepad >/dev/null 2>&1; then
		texteditor="mousepad"
	fi
}
FCT_texteditor

#changer splash
function FCT_splash_change() {
	if [ "$1" = "splash_change" ]; then
		#Convertir
		convert "$2" /tmp/multisystem/multisystem-splash-convert.png
		if [ "$(identify "/tmp/multisystem/multisystem-splash-convert.png" | awk '{print $2}')" != "PNG" ]; then
			zenity --error --text "$(eval_gettext "Erreur: impossible de détecter Type:PNG dans votre sélection.")"
			exit 0
		fi
		convert "/tmp/multisystem/multisystem-splash-convert.png" \
			-strip -density 72x72 \
			-resize 323x242 \
			"/tmp/multisystem/multisystem-splash-prevue.png"
		convert "/tmp/multisystem/multisystem-splash-convert.png" \
			-strip -density 72x72 \
			-resize 640x480 \
			"/tmp/multisystem/multisystem-splash.png"
		if [ "$(identify "/tmp/multisystem/multisystem-splash.png" | awk '{print $3}')" != "640x480" ]; then
			zenity --error --text "$(eval_gettext "Erreur: impossible de convertir en 640x480.")"
			exit 0
		fi
		#copier splash
		mv -f "/tmp/multisystem/multisystem-splash-prevue.png" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash-prevue.png"
		mv -f "/tmp/multisystem/multisystem-splash.png" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png"
		cp -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash-prevue.png" "$HOME"/.local/share/icons/hicolor/48x48/apps/multisystem-splash-prevue.png
		#splash pour grub4dos
		convert -resize 640x480 -colors 14 \
			"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png" \
			"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.xpm.gz"
		exit 0
	fi
}

#splash_reset
function FCT_splash_reset() {
	if [ "$1" == "splash_reset" ]; then
		rm "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png"
		rm "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash-prevue.png"
		rm "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.xpm.gz"
		cp "${dossier}/boot/splash/not_available.png" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash-prevue.png"
		cp -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/not_available.png" "$HOME"/.local/share/icons/hicolor/48x48/apps/multisystem-splash-prevue.png
		exit 0
	fi
}

#splash_revert
function FCT_splash_revert() {
	if [ "$1" == "splash_revert" ]; then
		cp -f "${dossier}/boot/splash/splash.png" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png"
		cp -f "${dossier}/boot/splash/splash-prevue.png" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash-prevue.png"
		cp -f "${dossier}/boot/splash/splash-prevue.png" "$HOME"/.local/share/icons/hicolor/48x48/apps/multisystem-splash-prevue.png
		cp -f "${dossier}/boot/splash/splash.xpm.gz" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.xpm.gz"
		exit 0
	fi
}

#splash_flip
function FCT_splash_flip() {
	if [[ "$1" == "splash_flip" && -e "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png" ]]; then
		mogrify -flip "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png"
		mogrify -flip "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash-prevue.png"
		mogrify -flip "$HOME"/.local/share/icons/hicolor/48x48/apps/multisystem-splash-prevue.png
		#splash pour grub4dos
		convert -resize 640x480 -colors 14 \
			"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png" \
			"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.xpm.gz"
		exit 0
	fi
}

#splash flop
function FCT_splash_flop() {
	if [[ "$1" == "splash_flop" && -e "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png" ]]; then
		mogrify -flop "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png"
		mogrify -flop "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash-prevue.png"
		mogrify -flop "$HOME"/.local/share/icons/hicolor/48x48/apps/multisystem-splash-prevue.png
		#splash pour grub4dos
		convert -resize 640x480 -colors 14 \
			"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.png" \
			"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/splash/splash.xpm.gz"
		exit 0
	fi
}

#exporter les fonftions
export -f FCT_splash_flop FCT_splash_flip FCT_splash_revert FCT_splash_reset FCT_splash_change
