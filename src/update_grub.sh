#! /bin/bash

echo -e " \E[37;44m\033[1m $(eval_gettext 'Patience Mise à jour de grub') \033[0m"
echo -e " \E[37;44m\033[1m $(eval_gettext 'Grub nécéssite les droits d\047administrateur') \033[0m"
sudo echo
#dosfsck -n $(cat /tmp/multisystem/multisystem-selection-usb)

#Pause de 5 secondes ...
declare -i i=5
while ((i > 0)); do
	echo -e " \E[37;44m\033[1m wait $i \033[0m"
	((i = i - 1))
	sleep 1
done

#remonter au cas ou ?
sudo mount -o remount,rw $(cat /tmp/multisystem/multisystem-selection-usb)
#sudo mount -o remount,rw,sync $(cat /tmp/multisystem/multisystem-selection-usb)
#gvfs-mount -d $(cat /tmp/multisystem/multisystem-selection-usb) 2>/dev/null

#echo Attente
#read

#Stop si pas accès à la clé USB.
echo test >"$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.1234"
if [ -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.1234" ]; then
	rm "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/.1234"
else
	zenity --info --text "Error: please unplug/replug your USB key."
	exit 0
fi

#Sauver précédent fichier de conf de Grub2
if [ "$(grep "#MULTISYSTEM_ST" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null | wc -l)" = "2" ]; then
	cp -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.save.cfg"
fi

#Nouvelle syntaxe de Grub2! version ==> 2.0 (Trusty Tahr version est avec -V)
if [ "$(sudo grub-install -V 2>/dev/null | grep -E '(2.0)')" ]; then
	sed -i "s@search --no-floppy --fs-uuid --set @search --no-floppy --fs-uuid --set=root @g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null

#Ancienne syntaxe de Grub2! version ==> 1.96 ... 1.98
elif [ "$(sudo grub-install -v | grep -E '(1.96)|(1.97)|(1.98)')" ]; then
	sed -i "s@search --no-floppy --fs-uuid --set=root @search --no-floppy --fs-uuid --set @g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null

#Nouvelle syntaxe de Grub2! version ==> 1.99
elif [ "$(sudo grub-install -v | grep -E '(1.99)')" ]; then
	sed -i "s@search --no-floppy --fs-uuid --set @search --no-floppy --fs-uuid --set=root @g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null

#Nouvelle syntaxe de Grub2! version ==> 2.0
elif [ "$(sudo grub-install -v | grep -E '(2.0)')" ]; then
	sed -i "s@search --no-floppy --fs-uuid --set @search --no-floppy --fs-uuid --set=root @g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null

else
	zenity --error --text "Error: grub-pc version ?"
	sed -i "s@search --no-floppy --fs-uuid --set @search --no-floppy --fs-uuid --set=root @g" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null
fi

#Décharger le module videotest pour Ubuntu Maverick Meerkat
sed -i "s/^insmod videotest/#insmod videotest/" $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg
#http://www.linuxpedia.fr/doku.php/expert/grub2

#Supprimer 40_multiboot si présent, car a changé de nom !
sudo rm /etc/grub.d/40_multiboot 2>/dev/null

#Copier/maj Virtualisation win VBox
if [ ! "$(grep 'VERSION=1.1' "$(cat /tmp/multisystem/multisystem-mountpoint-usb 2>/dev/null)/multisystem.bat" 2>/dev/null)" ]; then
	cp -f "${dossier}/divers/multisystem.bat" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/multisystem.bat"
	#Numéro de série
	sudo ${dossier}/divers/vin_vbox.sh
fi

#Sauver mbr origine
if [ ! -e "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/multisystem.bs.save" ]; then
	sudo dd if="$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')" of="$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/multisystem.bs.save" bs=446 count=1
	#reset mbr
	#if [ "$(which tazpkg)" ]; then
	#sudo dd if="/usr/share/boot/mbr.bin" of="$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')"
	#else
	#sudo dd if="/usr/lib/syslinux/mbr.bin" of="$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')"
	#fi
fi

#Mettre flag boot sur on
sudo parted -s "$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')" set 1 boot on

#Insérer UUID dans conf de Grub2
if [ "$(grep 'uuid-uuid-uuid' "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg" 2>/dev/null)" ]; then
	sed -i "s@uuid-uuid-uuid@$(cat /tmp/multisystem/multisystem-selection-uuid-usb)@" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg"
fi

#virer les lignes vides dans conf de Grub2
sed -i "/^$/d" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/grub.cfg"

#Option --force de Grub2 dispo après Jaunty!
if [ "$(grub-install -h 2>/dev/null | grep '\--force')" ]; then
	options_grub2=" --force "
fi

#Mettre à jour grub4dos
if [ "$(diff "${dossier}"/boot/grub.exe "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub.exe" 2>/dev/null)" ]; then
	cp -f "${dossier}"/boot/grub.exe "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub.exe"
fi

sync
#echo Attente
#read

#Installer Grub2
echo -e " \E[37;44m\033[1m Update Grub2 \033[0m"
#
#i386
if [ -d "/usr/lib/grub/i386-pc" ]; then
	sudo grub-install --root-directory="$(cat /tmp/multisystem/multisystem-mountpoint-usb)" --efi-directory="$(cat /tmp/multisystem/multisystem-mountpoint-usb)/EFI/BOOT" \
		--no-floppy ${options_grub2} --removable --no-uefi-secure-boot --target=i386-pc --directory=/usr/lib/grub/i386-pc --recheck "$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')"
	echo i386 ...

#x86_64-efi
elif [ -d "/usr/lib/grub/x86_64-efi" ]; then
	sudo grub-install --root-directory="$(cat /tmp/multisystem/multisystem-mountpoint-usb)" --efi-directory="$(cat /tmp/multisystem/multisystem-mountpoint-usb)/EFI/BOOT" \
		--no-floppy ${options_grub2} --removable --no-uefi-secure-boot --target=i386-pc --directory=/usr/lib/grub/x86_64-efi --recheck "$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')"
	# si grub-efi installé et table de partition gpt, boot UEFI possible, ajouter fonction ?
	echo x86_64-efi ...

#Spécial install wubi Natty !
elif [[ "$(which grub-install.real)" && "$(grep 'loop=/ubuntu/disks/root.disk' <<<"$(cat /proc/cmdline)" 2>/dev/null)" ]]; then
	sudo grub-install.real --root-directory="$(cat /tmp/multisystem/multisystem-mountpoint-usb)" --efi-directory="$(cat /tmp/multisystem/multisystem-mountpoint-usb)/EFI/BOOT" \
		--no-floppy ${options_grub2} --recheck "$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')"
	echo wubi ...

#Other
else
	sudo grub-install --root-directory="$(cat /tmp/multisystem/multisystem-mountpoint-usb)" --efi-directory="$(cat /tmp/multisystem/multisystem-mountpoint-usb)/EFI/BOOT" \
		--no-floppy ${options_grub2} --recheck "$(cat /tmp/multisystem/multisystem-selection-usb | sed 's/[0-9]//')"
	echo Other ...
fi

sync
#echo Attente
#read

#sur arch 64 bits boot.img est mis dans dossier: /boot/grub/i386-pc/boot.img
if [ -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/i386-pc/boot.img" ]; then
	cp -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/i386-pc/boot.img" "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/grub/boot.img"
fi
echo Copy boot.img
#echo Attente
#read

#Installer Syslinux
if [ ! -f "$(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ldlinux.sys" ]; then
	echo -e " \E[37;44m\033[1m Update Syslinux \033[0m"
	#Copier/maj des fichiers Syslinux
	if [ "$(which tazpkg)" ]; then
		sudo unlzma /usr/share/boot/chain.c32.lzma 2>/dev/null
		sudo unlzma /usr/share/boot/vesamenu.c32.lzma 2>/dev/null
		sudo unlzma /usr/share/boot/hdt.c32.lzma 2>/dev/null
		sudo unlzma /usr/share/boot/ifplop.c32 2>/dev/null
		cp -f /bin/syslinux $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /boot/isolinux/reboot.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /usr/share/boot/chain.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /usr/share/boot/vesamenu.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /usr/share/boot/hdt.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /usr/share/boot/ifplop.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
	else
		cp -f /usr/lib/syslinux/menu.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /usr/lib/syslinux/chain.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /usr/lib/syslinux/hdt.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
		cp -f /usr/lib/syslinux/memdisk $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /usr/lib/syslinux/reboot.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /usr/lib/syslinux/vesamenu.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		cp -f /usr/lib/syslinux/ifplop.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/
		#Ajout pour nouveau chemin ? ==> https://forum.ubuntu-fr.org/viewtopic.php?id=1229491&p=52
		cp -f /usr/lib/syslinux/modules/bios/menu.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
		cp -f /usr/lib/syslinux/modules/bios/chain.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
		cp -f /usr/lib/syslinux/modules/bios/hdt.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
		cp -f /usr/lib/syslinux/modules/bios/memdisk $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
		cp -f /usr/lib/syslinux/modules/bios/reboot.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
		cp -f /usr/lib/syslinux/modules/bios/vesamenu.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
		cp -f /usr/lib/syslinux/modules/bios/ifplop.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
		#http://askubuntu.com/questions/612746/ubuntu-live-usb-wont-boot-failed-to-load-com32-file-menu-c32
		cp -f /usr/lib/syslinux/modules/bios/libcom32.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
		cp -f /usr/lib/syslinux/modules/bios/libutil.c32 $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/ 2>/dev/null
	fi

	#New ?
	#http://www.syslinux.org/wiki/index.php?title=Config#UI
	rsync -avS --progress /usr/lib/syslinux/modules/bios/. $(cat /tmp/multisystem/multisystem-mountpoint-usb)/boot/syslinux/.
	sleep 1

	#Demonter
	sudo umount $(cat /tmp/multisystem/multisystem-selection-usb)

	# Test if the version of Syslinux supports '-i' argument
	if [ "$(grep '\--install' <<<$(syslinux 2>&1 &) 2>/dev/null)" ] then
		sudo syslinux -i -d /boot/syslinux $(cat /tmp/multisystem/multisystem-selection-usb)
	else
		sudo syslinux -d /boot/syslinux $(cat /tmp/multisystem/multisystem-selection-usb)
	fi
	sync

	#Réparer
	sudo dosfsck -a -w -v $(cat /tmp/multisystem/multisystem-selection-usb)
	sync

	function FCT_REDIM() {
		calc_fatresize="$(sudo fatresize -i $(cat /tmp/multisystem/multisystem-selection-usb) | grep '^Max size' | awk '{print $3}')"
		echo $((${calc_fatresize} / 1000 / 1000 - 1))

		#Diminuer un poil
		sudo fatresize -p -s $((${calc_fatresize} / 1000 / 1000 - 256))M $(cat /tmp/multisystem/multisystem-selection-usb)
		sync

		#Remettre à la dimmension
		sudo fatresize -p -s $((${calc_fatresize} / 1000 / 1000 - 1))M $(cat /tmp/multisystem/multisystem-selection-usb)
		sync

		#Réparer
		sudo dosfsck -a -w -v $(cat /tmp/multisystem/multisystem-selection-usb)
		sync
	}
	#FCT_REDIM

	#Remonter
	#gvfs-mount -d $(cat /tmp/multisystem/multisystem-selection-usb) 2>/dev/null
	#
	#Remplacement de gvfs-mount par "libglib2.0-bin" ==> gio
	gio mount -d $(cat /tmp/multisystem/multisystem-selection-usb) 2>/dev/null
	#
	sync

	#Seule methode qui fonctionne pour booter Syslinux avec grub2 1.98, chainer sur le mbr ...
	sudo dd if="$(cat /tmp/multisystem/multisystem-selection-usb)" of="$(cat /tmp/multisystem/multisystem-mountpoint-usb 2>/dev/null)/boot/img/syslinux.mbr" bs=512 count=1
	sync
fi

#Vérifier si volume est ok ?
#sudo dosfsck -n $(cat /tmp/multisystem/multisystem-selection-usb)
#if [ "$?" != "0" ]; then
#Réparer automatiquement
#sudo dosfsck -a -w -v $(cat /tmp/multisystem/multisystem-selection-usb)
#Réparer ?
#if [ "$?" != "0" ]; then
#zenity --error --text "ATTENTION ! le volume $(cat /tmp/multisystem/multisystem-selection-usb) nécéssite une réparation manuelle."
#fi
#fi

#echo Attente
#read
