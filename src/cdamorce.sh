#echo -e "\E[37;44m\033[1m ooo \033[0m"
#echo -e "\033[1;47;31m ooo \033[0m"

#Récupérer les options
option1="$(cat /tmp/multisystem/multisystem-option1 2>/dev/null)"
option2="$(cat /tmp/multisystem/multisystem-option2 2>/dev/null)"
option3="$(cat /tmp/multisystem/multisystem-option3 2>/dev/null)"

#Inclure common.sh (fonctions communes)
#export -f FCT_STOPTIMER
#export -f FCT_ERROR
#export -f FCT_END
#export -f FCT_KILL
#export -f FCT_UPDATEGRUB
#export -f FCT_ADDCOMMON
#export -f FCT_UPDATETREE
. ./common.sh

#Faire test présence de PloP et message si pas présent
if [ ! -f "$HOME"/.multisystem/nonfree/bootcd/boot/plpbt.img ]; then
	echo -e "\033[1;47;31m $(eval_gettext 'Veuillez installer \"PLoP Boot Manager\"\naprès avoir accepté sa licence,\nPloP est un freeware non libre.\n\nInstallation accessible via le menu: Non-libre ==> Installer partie non libre ==> Télécharger PloP Boot Manager') \033[0m"
	echo
	FCT_ERROR

else
	echo -e "\E[37;44m\033[1m $(eval_gettext 'Veuillez saisir votre mot de passe d\047administrateur') \033[0m"
	mkdir /tmp/multisystem/multisystem-mountpoint-plop 2>/dev/null
	sudo mount -o loop,rw "$HOME"/.multisystem/nonfree/bootcd/boot/plpbt.img /tmp/multisystem/multisystem-mountpoint-plop
	#Activer usb par defaut!
	sudo "$HOME"/.multisystem/nonfree/plpcfgbt cnt=on cntval=3 dbt=usb /tmp/multisystem/multisystem-mountpoint-plop/plpbt.bin
	#echo Attente
	#read
	sudo umount /tmp/multisystem/multisystem-mountpoint-plop
	rmdir /tmp/multisystem/multisystem-mountpoint-plop
	cd "$HOME"/.multisystem/nonfree
	genisoimage -r -b boot/plpbt.img -c boot/boot.catalog -o "$HOME"/cd-boot-liveusb.iso -V plop_bootmanager bootcd
	#echo Attente
	#read

	echo -e '\E[37;44m'"\033[1m $(eval_gettext "Veuillez graver image du cd\nChemin: \$HOME/cd-boot-liveusb.iso\n(clic droit, graver sur le disque...)") \033[0m"
	zenity --info --text "<b>$(eval_gettext "l\047image du cd d\047amorce est disponible,")
$(eval_gettext "Chemin"): \"$HOME/cd-boot-liveusb.iso\"
$(eval_gettext "(clic droit, graver sur le disque...)")

$(eval_gettext "Avant démarrer votre PC si besoin est,")
$(eval_gettext "insérez le CD ainsi que votre clé USB")</b>" &
fi
FCT_UPDATETREE
FCT_END
