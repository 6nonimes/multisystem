#echo -e "\E[37;44m\033[1m ooo \033[0m"
#echo -e "\033[1;47;31m ooo \033[0m"

#Récupérer les options
option1="$(cat /tmp/multisystem/multisystem-option1 2>/dev/null)"
option2="$(cat /tmp/multisystem/multisystem-option2 2>/dev/null)"
option3="$(cat /tmp/multisystem/multisystem-option3 2>/dev/null)"

#Inclure common.sh (fonctions communes)
#export -f FCT_STOPTIMER
#export -f FCT_ERROR
#export -f FCT_END
#export -f FCT_KILL
#export -f FCT_UPDATEGRUB
#export -f FCT_ADDCOMMON
#export -f FCT_UPDATETREE
. ./common.sh

FCT_UPDATEGRUB
FCT_UPDATETREE
FCT_END
