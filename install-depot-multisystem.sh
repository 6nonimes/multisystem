#! /bin/bash
exec >& >(tee -a /tmp/debug-install-depot-multisystem.txt)

#	│▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒│
#	│          install-depot-multisystem.sh              │
#	│              written by François Fabre             │
#	│               MultiSystem LiveUSB                  │
#	│▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒│

# Mr Fabre François @frafa
# E-Mail: liveusb@gmail.com (In French Please) !
# http:\\liveusb.info/

# License: GNU General Public License (GPL)
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# http://www.gnu.org/licenses/gpl.html

# This script basically does two things
# Adds a new apt repository to /etc/sources.list : "deb http:\\liveusb.info/multisystem/depot all main"
# Installs the package "multisystem" from the new repository

# Under KDE, enable the "universe" respository and install zenity
if [ "$(sudo -A cat grep '^%admin ALL=(ALL) NOPASSWD: ALL' /etc/sudoers 2>/dev/null)" ]; then
	echo
	if [ "$(which software-properties-kde)" ]; then
		sudo software-properties-kde --enable-component universe
		if [ ! "$(which zenity)" ]; then
			xterm -title 'Install zenity' -e "sudo apt-get update && sudo apt-get install -q -y zenity"
		fi
	fi
fi

# Test for the presense of required packages
errorlist=()
testlist="apt-get sudo lsb_release tee zenity nohup xterm dpkg"
for i in ${testlist}; do
	if [ ! "$(which "$i")" ]; then
		errorlist=(${errorlist[@]} $i)
	fi
done

# Exit if any packages were missing
if [ "$(echo "${errorlist[@]}")" ]; then
	echo -e "\033[1;47;31m Error: ${errorlist[@]} \033[0m"
	formatting="${errorlist[@]}"
	zenity --error --text "Error: ${formatting}" 2>/dev/null
	exit 0
fi

# if not sudo
if [ ! "$SUDO_USER" ]; then
	zenity --info --text "<b>Installing multisystem requires administrator rights.</b>"
	xterm -e "sudo "$0""
	exit 0
fi

# chmod 777 /media
if [ "$(stat -c %a /media)" != "777" ]; then
	chmod 777 /media
fi

# Add test if already installed release!
if [ "$(dpkg -l | grep 'ii  multisystem')" ]; then
	zenity --info --text "<b>multisystem is already installed!</b>"
	exit 0
fi

# check that the user belongs to the group "disk"
if [ ! "$(grep ^disk /etc/group | grep "$SUDO_USER")" ]; then
	usermod -a -G disk "$SUDO_USER"
fi

# check that the user belongs to the group "adm"
if [ ! "$(grep ^adm /etc/group | grep "$SUDO_USER")" ]; then
	usermod -a -G adm "$SUDO_USER"
fi

# Enable "universe" repository
if [ "$(which software-properties-gtk)" ]; then
	# Under Gnome
	software-properties-gtk -e universe
elif [ "$(which software-properties-kde)" ]; then
	# Under Kde
	software-properties-kde --enable-component universe
fi

# For Ubuntu hardy activate the "hardy-backport" repository
if [ "$(lsb_release -cs)" == "hardy" ]; then
	echo
	# Under Gnome
	if [ "$(which software-properties-gtk)" ]; then
		software-properties-gtk -e hardy-backport
	# Under Kde
	elif [ "$(which software-properties-kde)" ]; then
		software-properties-kde --enable-component hardy-backport
	fi
fi

# Add the repository
if [ ! "$(grep '^deb http:\\liveusb.info/multisystem' /etc/apt/sources.list 2>/dev/null)" ]; then
	vardeb="## MultiSystem Repostiory\ndeb http:\\liveusb.info/multisystem/depot all main"
	echo -e "$vardeb" | tee -a "/etc/apt/sources.list"
fi

# Add public key for apt
sudo -u "$SUDO_USER" wget -q http:\\liveusb.info/multisystem/depot/multisystem.asc -O- | apt-key add -

# Reload sources
apt-get update

# Install the multisystem package
apt-get install -y --force-yes MultiSystem
apt-get -f install -y

# Check that it installed
if [ ! "$(dpkg -l | grep 'ii  multisystem')" ]; then
	zenity --error --text "<b>An error occurred</b>"
	exit 0
fi

# Make fusermount executable
chmod +x "/usr/bin/fusermount" 2>/dev/null
chmod +x "/bin/fusermount" 2>/dev/null

# LMDE mods
if [ "$(stat -c "%a" $(which fusermount))" = "4754" ]; then
	chmod 755 $(which fusermount)
fi
if [ "$(stat -c "%a" /media)" -lt "755" ]; then
	chmod 755 /media
fi
if [ "$(stat -c "%G" "$HOME/.local")" = "root" ]; then
	sudo chown $SUDO_USER:$SUDO_USER -R "$HOME"/.local
fi

#message
zenity --info --text "<b>Now to start the script please use\nthe menu: / Applications/Accessoires/MultiSystem.</b>"

# Start GUI
nohup sudo -u "$SUDO_USER" -i "/usr/local/share/multisystem/gui_multisystem.sh" &
sleep 1
exit 0
