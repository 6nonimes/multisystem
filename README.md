# MultiSystem

MulstiSystem is a multi Live CDs USB key creator written by François Fabre aka @frafa since 2009, before it was named multiboot.

This is a french software made in Montpellier !

[Sadly its development has stopped on 26/02/2022.](http://doc.ubuntu-fr.org/multisystem)

I wanted to have the sources here to maybe give a revival to this wonderfull project, let's see !

Big thanks to @frafa for putting the latest version of the bash source files on [SF](https://sourceforge.net/projects/multisystem/files/last_save/)

## Presentation EN (takenfrom SF)

Create your MultiSystem with MultiBoot LiveUSB simply.
You just have to Drag / Drop your. Iso on a window and that's it!

MultiSystem enables a LiveUSB to do everything, ideal for exploring different Linux distributions do not install them on your PC, and it seamlessly.

Or to install the Linux distribution of your choice on your PC much faster than via a LiveCD thanks to the speed of USB 2.0 ports.

Essential throughout the current range of netbooks that do not have CD.

Ideal for all Geek, anyone doing maintenance, your USB key with you always, and ready for use in a nomadic your friends in a cafe ...

~~See the current list of Linux distributions supported by MultiSystem.~~

Main interface that allows Drag/Drop iso files.

## Presentation FR (taken from SF)

Avec MultiSystem Créez votre LiveUSB MultiBoot simplement.
Vous avez juste à Glisser/Déposer vos fichiers .iso sur une fenêtre et c'est tout !

MultiSystem permet de réaliser un LiveUSB à tout faire, idéal pour découvrir différentes distributions linux sans les installer sur votre PC, et ce de manière fluide.

Ou pour installer la distribution linux de votre choix sur votre PC beaucoup plus rapidement que via un LiveCD grâce à la rapidité des ports USB 2.0.

Indispensable sur toute la gamme actuelle des netbooks qui ne possèdent pas de lecteur de CD.

Idéal pour tout Geek, toute personne faisant de la maintenance, votre clé usb toujours avec vous, et prête à être utilisée de manière nomade chez vos amis, dans un cybercafé...

~~Voir la liste actuelle des distributions linux supportées par MultiSystem.~~

Interface principale qui permet de Glisser/Déposer les fichiers iso.


They speak about mutlisystem (in french !) : 
 - https://sourceforge.net/projects/multisystem/
 - https://forum.ubuntu-fr.org/viewtopic.php?id=427605
 - https://www.docgreen.fr/2011/04/12/multisystem-ou-la-cle-usb-de-depannage-au-poil-de-barbe/
 - https://lea-linux.org/documentations/MultiSystem
 - https://wiki.facil.qc.ca/view/MultiSystem-LiveUSB-MultiBoot

/!\ Please do not visit liveusb.info website as this domain is not owned by @frafa any more and ublock is sending warnings ! 


## License

GPL V3 [EN](gpl-3.0.txt) - [FR](gpl-3.0-fr.txt)

## Authors

@frafa (liveusb@gmail.com)

@6nonimes

## Project status

Waiting for new fixes to be able to run with latest versions of grub, latest live cds.

In the meantime :
 - if you are on a hurry you can use https://ventoy.net/
 - if you have time, you can contribute to revive it !
 
## Roadmap

 - [x] Save latest @frafa work in Legacy branch
 - [ ] Translate revisions.txt into Change.log
 - [x] Remove all links to http:\\liveusb.info/
 - [ ] Full code review
 - [ ] Code cleanup : 
    - [x] Don't change logik, just add indent (thanks shfmt -l -w *.sh)
    - [x] Move all sh files to src folder
    - [ ] Try to reduce lines width?
 - [ ] Add coding development guidelines start here : https://sharats.me/posts/shell-script-best-practices/
 - [ ] Adapt to new grub versions
 - [ ] Adapt to new iso?
 - [ ] Check all links in list.txt for url changes and dead links.
 - [ ] Add CI
 - [ ] Fix : The terminal (VteTerminal) widget requires a version of gtkdialog built with libvte !
 - [ ] gtkdialog is not maintained any more? (last version is 0.8.3 from 2014)
    - [ ] https://github.com/puppylinux-woof-CE/gtkdialog/ seems more up to date !
    - [ ] Convert all screens ? 
    - [ ] add gtkdialog-0.8.3 building? (only ./autogen.sh && make && make install worked for me and I add to install install texinfo to be able to build !)
 - [ ] Add wiki
 - [ ] Reduce README
 - [ ] Create .appimage?
 - [ ] Add todos ;)
